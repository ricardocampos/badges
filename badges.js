module.exports = {
	isBadgeInterestedInEvent: function(badgeName,eventName){
		if (badgeName=='firstLogin'){
			return eventName=="My.CTM.Domain.Events.MainAccountHolderSetForFirstTime";
		} else if (badgeName=='personalDetailsComplete'){
			return eventName=="My.CTM.Domain.Events.AccountAssociate.AccountAssociateTelephoneNumberChanged";
		} else if(badgeName=='addedAssociate'){
			return eventName=="My.CTM.Domain.Events.AccountAssociateCreated";
		} else if(badgeName=='maritalStatusChanged'){
			return eventName=="My.CTM.Domain.Events.AccountAssociate.AccountAssociateMaritalStatusChanged";
		}
		return false;
	}
};

var express = require('express'),
    app = express(),
    nano = require('nano')('http://localhost:5984');

var bodyParser = require('body-parser');
var repo=require('./repo.js');
var achievements=require('./badges.js');

nano.db.create('badges', function(err, body) {
  if (!err) {
    console.log('badges db created');
  }
});


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.get('/:accountId', function(req, res) {
	console.log(req.params.accountId);
	repo.getBadgesForUser(req.params.accountId, function(badges) {
		res.send(badges);
	});
});

//Post me your shizzle
app.post('/:accountId',function(req,res){
	console.log(req.params.accountId);
	// have an eventName
	//req.body.eventName
	 
	repo.getBadgesForUser(req.params.accountId, function(badges) {
		console.log('badges= '+badges);
		// if no badges then create new collection.
		if ( badges == 'missing') {
			badges=JSON.parse('{"badges": [{"badgeName": "firstLogin", "achieved":false},{"badgeName": "maritalStatusChanged", "achieved":false},{"badgeName": "personalDetailsComplete", "achieved":false},{"badgeName": "addedAssociate", "achieved":false}]}'); 
			
		}
		for (var i=0;i < badges.badges.length; i++)
		{
			console.log('Badge '+badges.badges[i].badgeName+' for event '+ req.body.eventName);
			if (achievements.isBadgeInterestedInEvent(badges.badges[i].badgeName,req.body.eventName)){
				badges.badges[i].achieved=true;
			}
		}
		// else update badges and re-persist.
		console.log(badges);
   		repo.saveDocument(req.params.accountId,badges); 
	});

	res.send('/'+req.params.accountId);
});

var server = app.listen(80, function() {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);
});

